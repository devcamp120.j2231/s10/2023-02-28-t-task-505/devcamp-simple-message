//Khai báo thư viên express
const express = require("express");

//Khởi tạo app express
const app = express();

//Khai báo cổng
const port = 8000;

//khai báo để sử dụng body string
app.use(express.json());

//************* Task 505.10 ************/
//Khai báo request trả về message
app.get('/', (request, response) => {
    var date = new Date();
    console.log(date);
    response.status(200).json({
        message:`Hôm nay là ngày ${date.getDate()} tháng ${date.getMonth()} năm ${date.getFullYear()}`
    })    
})

//************* Task 505.20 ************/

app.get('/get-method', (req, res) => {
    console.log(req.method);
    res.status(200).json({
        message:"Get-Method"
    })
})

app.post('/post-method', (req, res) => {
    console.log(req.method);
    res.status(201).json({
        message:"Post-Method"
    })
})

app.put('/put-method', (req, res) => {
    console.log(req.method);
    res.status(201).json({
        message:"Put-Method"
    })
})

app.delete('/delete-method', (req, res) => {
    console.log(req.method);
    res.status(201).json({
        message:"Delete-Method"
    })
})

//************* Task 505.20 ************/
//tham số trên đường dẫn
app.get('/get-param/:param1/:para2/:p3', (req, res) => {
    var param1 = req.params.param1;
    var param2 = req.params.para2;
    var param3 = req.params.p3;
    console.log(req.params);
    res.status(200).json({
        param1,
        param2,
        param3
    })
})

//tham số dạng query string get-query?param1=value1&param2=value2...
app.get('/get-query', (req, res) => {
    var query = req.query;
    console.log(query);
    res.json({
        query
    })
})


//tham số dạng body json
app.post('/post-body', (req, res) => {
    var body = req.body;
    console.log(body);
    res.json({
        body
    })
})


//Start app tại cổng 8000
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})
